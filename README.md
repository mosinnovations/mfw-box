# Mercer Financial Wellness Development Box

Development box for the MFW project, provisioned with Ansible, Docker and Vagrant.

## Install Instructions

1. Download and install [Vagrant](https://www.vagrantup.com/downloads.html)
1. Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
1. Clone this repository `git clone git@bitbucket.org:mosinnovations/mfw-box.git`
1. Cd into directory `mfw-box`
1. Type `vagrant up`
1. Wait for the box to provision, the script will exit once provision is successful.
1. Open browser window and navigate to `http://localhost:3000` for the mobile app
1. Open browser winow and navigate to `http://localhost:3200` for the admin app

User Credentials:

Regular User:

email: `nemi.act37@test.com`
password: `Welcome1!`

Admin User:
email: `nemi.mfwdev2@yahoo.com`
passowrd: `Welcome6^`